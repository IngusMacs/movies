import React from 'react';
import Search from './components/search.js';
import './bootstrap-4.4.1-dist/css/bootstrap.min.css';
import './bootstrap-4.4.1-dist/css/bootstrap-grid.min.css';
import './bootstrap-4.4.1-dist/css/bootstrap-reboot.min.css';
import './App.css';

function App() {
  return (
    <div className="container">
      <Search/>
    </div>
  );
}

export default App;
