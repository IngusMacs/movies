import React, { Component } from 'react'
import { getDataFromService } from './utils.js';
import MovieList from './movieList.js'
import LargeImageModal from './largeImageModal'

class Search extends Component {

    constructor() {
        super();

        this.state = {
            movieList: [],
            imageFullSizeUrl: '',
            detailsId: null,
            searchTextId: null
        }

        this.child = React.createRef();
    }

    searchMovies = () => {

        let searchTextId

        // Get search text from textbox
        if (!document.getElementById('searchTextId') || !document.getElementById('searchTextId').value) {

            this.setState({
                movieList: [],
                searchTextId: null
            })

            return
        }

        searchTextId = document.getElementById('searchTextId').value;

        // Get shows from REST API
        getDataFromService('/search/shows?q=' + searchTextId)
            .then(data => {
                this.setState({
                    movieList: data,
                    searchTextId: searchTextId
                })
            })
    }

    componentDidMount = () => {

        // Get the modal
        const modal = document.getElementById("imageFullSizeModal");
        // Get the <img> element that closes the modal
        const modalImage = document.getElementById("modalImageId");
        const input = document.getElementById("searchTextId");

        // When the user clicks anywhere outside of the modal or image, close it
        modalImage.onclick = () => modal.style.display = "none"

        // Close modal if user click on image
        window.onclick = e => {
            if (e.target === modal) modal.style.display = "none"
        }

        input.addEventListener("keyup", e => {

            // Return is not Enter key is pressed
            if (e.keyCode !== 13) return

            // Cancel the default action, if needed
            e.preventDefault()
            // Trigger the button element with a click
            document.getElementById("btnSearchMovies").click()
          })
    }

    render() {

        const { movieList, searchTextId } = this.state
        // Variable for list result
        const table = movieList.length ?
            <React.Fragment>
                <br></br>
                <h5>Movies</h5>
                <MovieList movieList={movieList} showImageFullSize={this.child.current.showImageFullSize} />
            </React.Fragment> :
            <div>{searchTextId ? 'No movies were found' : ''}</div>

        return (
            <div>
                <br>
                </br>
                <div className="input-group mb-3">
                    <input id='searchTextId' type="text" className="form-control" placeholder="Enter movie name" ></input>
                    <div className="input-group-append">
                        <button id="btnSearchMovies" className="btn btn-success" onClick={this.searchMovies}>Search</button>
                    </div>
                </div>

                {table}

                <LargeImageModal ref={this.child} />

            </div>
        )
    }
}

export default Search;