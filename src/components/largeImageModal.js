import React, { Component } from 'react'
import $ from 'jquery'

class LargeImageModal extends Component {

    showImageFullSize = imageFullSizeUrl => {

        this.setImageUrlToModal(imageFullSizeUrl)

        // Get imageFullSize modal
        const modal = document.getElementById("imageFullSizeModal")

        // Show modal
        modal.style.display = "block"
    }

    // Setting img url using JQuery
    setImageUrlToModal = url => $('#modalImageId').attr('src', url)

    render() {

        return (
            <div id="imageFullSizeModal" className="modal">

                <div className="modal-content largePicture">
                    <img id='modalImageId' alt='' className="img-fluid"
                        src=''>
                    </img>
                </div>
            </div>)
    }
}

export default LargeImageModal