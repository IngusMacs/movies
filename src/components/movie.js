import React, { Component } from 'react'

class Movie extends Component {

    constructor(props) {
        super(props);

        this.state = {
            movie: {},
            movieId: null
        }
    }

    // Calling parent function with user clicked show id
    showDetails = id => {

        if (!id) {
            return
        }

        this.props.getMovieId(id)
    }

    render() {

        const { movie, showImageFullSize } = this.props
        const { id, name, image, summary } = movie.show || {}

        return (
            <React.Fragment>
                <tr key={id} className='pointer'>
                    <td onClick={() => { this.showDetails(id) }}>{name}</td>
                    <td onClick={() => { this.showDetails(id) }}><div
                        dangerouslySetInnerHTML={{
                            __html: summary ? summary : '<p>No summary</p>'
                        }}></div>
                    </td>
                    <td className='example-popover smallPicture'>
                        <img alt={name}
                            src={image && image.medium ? image.medium : null}
                            onClick={() => showImageFullSize(image.original)}>
                        </img>
                    </td>
                </tr>
            </React.Fragment>
        )
    }
}

export default Movie;