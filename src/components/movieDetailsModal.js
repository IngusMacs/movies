import React, { Component } from 'react'
import { getDataFromService } from './utils.js';

class MovieDetailsModal extends Component {

    constructor(props) {
        super(props);

        this.state = {
            clickedSeasonId: null,
            episodes: []
        }
    }

    componentDidMount = () => {

        // Get the modal
        const modal = document.getElementById("detailsModal")

        // Get the <span> element that closes the modal
        const span = document.getElementsByClassName("close")[0]

        // When the user clicks on <span> (x), close the modal
        span.onclick = () => modal.style.display = "none"

        // When the user clicks anywhere outside of the modal, close it
        window.onclick = e => {
            if (e.target === modal) modal.style.display = "none"
        }
    }

    openModal = () => {

        // Get the modal
        const modal = document.getElementById("detailsModal")
        // Show modal
        modal.style.display = "block"
    }

    getEpisodes = e => {

        let seasonid;

        seasonid = +e.target.getAttribute('seasonid');

        // No seasonid was found
        if (!seasonid) return

        // If user clicks on season but details already opened then close details
        if (seasonid === this.state.clickedSeasonId && this.state.episodes.length) {
            this.setState({
                episodes: []
            })
            return
        }

        // Get show season episodes from REST API
        getDataFromService('/seasons/' + seasonid + '/episodes')
            .then(data => {
                this.setState({
                    episodes: data,
                    clickedSeasonId: +seasonid
                })
            })
    }

    render() {

        const { seasons } = this.props
        const { clickedSeasonId, episodes } = this.state

        let episodesJsx = ''
        const modalDetails = <React.Fragment>{seasons.map(season => {

            if (clickedSeasonId === season.id) {
                episodesJsx = <div className="container">
                    {episodes.map(episode => {
                        let image = episode.image && episode.image.medium ? episode.image.medium : ''
                        return <div key={episode.id} className='alert alert-light default'>
                            <div className="row">
                                <div><b>Episode: {episode.number} - {episode.name}</b></div>
                            </div>
                            <div className="row">
                                <div className="col-lg-8" dangerouslySetInnerHTML={{
                                    __html: episode.summary ? '<b>Summary:</b> ' + episode.summary : 'No summary'
                                }}></div>
                                <div className="col-lg-4">
                                    <img alt={episode.name} src={image} className='img-fluid'></img>
                                </div>
                            </div>
                        </div>
                    })}
                </div>
            } else {
                episodesJsx = ''
            }

            return (
                <div key={season.id} className="row" onClick={this.getEpisodes}>
                    <div seasonid={season.id} className="col-lg alert alert-dark pointer" role="alert">
                        <h4 seasonid={season.id}>Season: {season.number}</h4>
                        {episodesJsx}
                    </div>
                </div>
            )
        })}</React.Fragment>

        return (
            <div>
                {/* The details Modal */}
                <div id="detailsModal" className="modal">

                    <div className="modal-content">
                        <div className="modal-body">
                            <div className="row">
                                <span className="close" style={{ width: '100%', marginLeft: '3%' }}>&times;</span>
                            </div>
                            <div className="container">
                                {modalDetails}
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        )
    }
}

export default MovieDetailsModal;