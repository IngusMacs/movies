import { protocol, apiHost } from './const.js'
import axios from 'axios'

const getDataFromService = url => {

    // Reusable code for calling REST API
    // All service calls responses are validated in one place
    return axios.get(protocol + '://' + apiHost + url)
        .then(response => {

            if (!response || !response.data) return []

            console.log('data', response.data)

            return Array.isArray(response.data) ? response.data : [response.data]
        })
        .catch(error => {
            console.log('error', error);
            return [];
        })
}

export { getDataFromService }