import React, { Component } from 'react'
import { getDataFromService } from './utils.js';
import Movie from './movie.js';
import MovieDetailsModal from './movieDetailsModal';

class MovieList extends Component {

    constructor(prop) {
        super(prop);
        this.child = React.createRef();

        this.state = {
            movieList: [],
            movieId: null,
            seasons: [],
            movieDetails: {}
        }
    }

    // Function given to child to get user clicked movie id
    getMovieId = id => this.getDetails(id)

    getDetails = id => {

        let that = this

        // Get show seasons from REST API
        getDataFromService('/shows/' + id + '/seasons')
            .then(data => {
                this.setState({ seasons: data }, () => {
                    that.openModal()
                })
            })
    }

    // Child function to open movie details modal 
    openModal = () => this.child.current.openModal()

    render() {

        const { movieList, showImageFullSize } = this.props

        return (
            <div className='table-responsive'>
                <table className="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th scope="col">Name</th>
                            <th scope="col">Description</th>
                            <th scope="col">Cover</th>
                        </tr>
                    </thead>

                    <tbody>
                        {movieList.map((movie, index) => {
                            if (!movie || !movie.show) {
                                return null;
                            }
                            return <Movie key={movie.show.id} movie={movie} showImageFullSize={showImageFullSize}
                                getMovieId={this.getMovieId} />
                        })}
                    </tbody>

                </table>

                <MovieDetailsModal ref={this.child} showDetails={this.showDetails} seasons={this.state.seasons} />

            </div>
        )
    }
}

export default MovieList;